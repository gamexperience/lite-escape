<?php

$baseTemplate = file_get_contents('lite_escape_template.html');
preg_match_all('/\{{2}(.*?)\}{2}/is',$baseTemplate,$match);


//read translation file
$translationJson = json_decode(file_get_contents('translation.json'));


foreach ($translationJson->_support as $key => $language) {        
    $htmlFileBuild = $baseTemplate;
    $htmlFileBuild = str_replace('"../','"', $htmlFileBuild);
    foreach ($match[1] as $key => $template) {
        if(property_exists($translationJson->_translations, $template)){
            if(property_exists($translationJson->_translations->{$template}, $language)){
                $htmlFileBuild = str_replace('{{'.$template.'}}',$translationJson->_translations->{$template}->{$language}, $htmlFileBuild);
            }else{
                print_r('Missing language "' . $language . '" for key: ' . $template . PHP_EOL);
            }
        }else{
            print_r('Missing template key: ' . $template . PHP_EOL);
        }
        
    }

    file_put_contents('../lite_escape_'.$language.'.html', $htmlFileBuild);
}

