//vars
const channel = new BroadcastChannel('lite_escape_channel');
const messageControl = document.querySelector('#inputMessage');
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

if(notif_hints_active){
    var audioHint = new Audio('assets/media/' + notif_hints_file);
}

if(notif_video_active) {
    var audioVideo = new Audio('assets/media/' + notif_video_file);
}

var timer = 3600;
var display = '';
var isPaused = true;
var interval = setInterval(countdown, 1000);
var videoModal = new bootstrap.Modal(document.getElementById('videoModal'));

//adminMode
if (!urlParams.has('admin')) {
    document.getElementById('admin').setAttribute('class', 'd-none');
}else{
    document.getElementById('game').setAttribute('class', 'd-none');
}

//functions
function countdown() {
    if (!isPaused) {
        timer--;
        if (timer == 0) {
            channel.postMessage({ timer: 'stop' });
            clearInterval(interval);
        }
    }

    display = new Date(timer * 1000).toISOString().substr(11, 8)
    document.getElementById('clockplayer').innerHTML = display;
    channel.postMessage({ clockupdate: display });

}

function resetName() {
    document.getElementById("hint").innerHTML =DEFAULT_NAME;
}

function addToLog(msg, color='info') {
    var dateValue = new Date();
    msg = dateValue.getHours() + ':' + dateValue.getMinutes() + ':' + dateValue.getSeconds() + ' > ' + msg;
    var elemDiv = document.createElement("div");
        elemDiv.setAttribute('class', 'alert alert-'+color);
        elemDiv.setAttribute('role', 'alert');
        elemDiv.innerHTML = msg;
    var container = document.getElementById('logs');
    container.insertBefore(elemDiv, container.firstChild);    
    console.log(msg);
}

//buttons     
document.querySelector('#hintSend').addEventListener('click', (e) => {
    e.preventDefault();
    channel.postMessage({ hint: messageControl.value });
    addToLog( '💡 ' + messageControl.value, 'info' )
})

document.querySelector('#hintHide').addEventListener('click', (e) => {
    e.preventDefault();
    document.getElementById('inputMessage').value = ''; 
    channel.postMessage({ hint: false });
})

document.querySelector('#timerStart').addEventListener('click', (e) => {
    e.preventDefault();
    channel.postMessage({ timer: 'start' });
    addToLog(' Start ', 'success' );
})

document.querySelector('#timerPause').addEventListener('click', (e) => {
    e.preventDefault();
    channel.postMessage({ timer: 'pause' });
    addToLog(' Pause ', 'warning' );
})

document.querySelector('#timerReset').addEventListener('click', (e) => {
    e.preventDefault();
    channel.postMessage({ timer: 'reset' });
    addToLog(' Reset ', 'danger' );
})

document.querySelector('#timerAdd').addEventListener('click', (e) => {
    e.preventDefault();
    channel.postMessage({ timer: 'add' });
    addToLog(' + 1 MIN ', 'primary' );
})

document.querySelector('#timerRemove').addEventListener('click', (e) => {
    e.preventDefault();
    channel.postMessage({ timer: 'remove' });
    addToLog(' - 1 MIN ', 'primary' );
})

document.querySelector('#clear').addEventListener('click', (e) => {
    e.preventDefault();    
    document.getElementById('logs').innerHTML = '';
    addToLog('clear', 'danger' );
})

//listeners
channel.onmessage = event => {

    if ("hint" in event.data) {
        if (event.data.hint) {
            document.getElementById("hint").innerHTML = event.data.hint;
            if(notif_hints_active){
                audioHint.play();
            }            
            console.log('event hint display');
        } else {
            document.getElementById("hint").innerHTML = "";
            console.log('event hint remove');
        }

    }

    if ("video" in event.data) {
        if (event.data.video) {
            if(notif_video_active){
                audioVideo.play();
            }  
            if(document.getElementById('theVideo') !== null){
                document.getElementById('theVideo').remove();
            }
            var elemVideo = document.createElement("video");
                elemVideo.setAttribute("id", 'theVideo');
                elemVideo.autoplay = true;
                elemVideo.src = 'videos/' + event.data.video;
            document.getElementById('videoWrapper').appendChild(elemVideo);
            videoModal.show()
            elemVideo.play();
            console.log('video start');
        } else {            
            console.log('video stop');
            videoModal.hide();
            document.getElementById('theVideo').remove();
            
        }

    }

    if ("clockupdate" in event.data) {
        if (event.data.clockupdate) {
            document.getElementById('clockadmin').innerHTML = event.data.clockupdate;
        }
    }

    if ("timer" in event.data) {

        switch (event.data.timer) {
            case 'start':
                isPaused = false;
                console.log('event timer start');
                break;
            case 'pause':
                isPaused = true;
                console.log('event timer pause');
                break;
            case 'reset':
                isPaused = true;
                timer = 3600;
                console.log('event timer reset');
                break;
            case 'remove':
                timer -= 60;
                console.log('event timer remove');
                break;
            case 'add':
                timer += 60;
                console.log('event timer add');
                break;

            default:
                break;
        }
    }
}

// Logos loader

if (!urlParams.has('admin') && logos.length > 0) {
    imgCount = 0;
    logos.forEach(logo => {
        imgCount++;
        generatedID = 'img_'+imgCount;
        var elemDiv = document.createElement("div");
            elemDiv.setAttribute("id", generatedID);
            elemDiv.setAttribute("class", 'col');
        var elemImg = document.createElement("img");
            elemImg.setAttribute("src", "logos/" + logo);
            elemImg.setAttribute("alt", logo);
            elemImg.setAttribute("class", 'img-fluid');
            document.getElementById('logos').appendChild(elemDiv);
            document.getElementById(generatedID).appendChild(elemImg);
    });
}

// Hints loader

if (urlParams.has('admin') && hints.length > 0) {

    document.getElementById('hintList').setAttribute("class", 'd-block');

    hintCount = 0;    
    hints.forEach(hint => { console.log(hint);
        hintCount++;
        generatedID = 'hint_'+hintCount;
        var elemDiv = document.createElement("a");
            elemDiv.setAttribute("id", generatedID);
            elemDiv.innerHTML = hint;
            elemDiv.setAttribute("class", 'hintbutton btn btn-outline-info m-1');
            document.getElementById('hintList').appendChild(elemDiv);
    });

    //listener

    document.querySelectorAll('.hintbutton').forEach(hintbutton => {
        hintbutton.addEventListener('click', (e) => {
            e.preventDefault();
            document.getElementById('inputMessage').value = ''; 
            document.getElementById('inputMessage').value = e.target.innerText;        
        })
    });
}

// Videos loader

if (urlParams.has('admin') && videos.length > 0) {

    document.getElementById('videoList').setAttribute("class", 'd-block');
    videoCount = 0;    
    videos.forEach(video => { console.log(video);
        videoCount++;
        generatedID = 'video_'+videoCount;
        var elemDiv = document.createElement("a");
            elemDiv.setAttribute("id", generatedID);
            elemDiv.innerHTML = video;
            elemDiv.setAttribute("class", 'videobutton btn btn-outline-warning m-1');
            document.getElementById('videoList').appendChild(elemDiv);
    });

    //listener

    document.querySelectorAll('.videobutton').forEach(videobutton => {
        videobutton.addEventListener('click', (e) => {
            e.preventDefault();
            channel.postMessage({ video: e.target.innerText });
            addToLog( '🎬 ' + e.target.innerText, 'warning' )       
        })
    });

    document.querySelectorAll('.videostop').forEach(videobutton => {
        videobutton.addEventListener('click', (e) => {
            e.preventDefault();
            channel.postMessage({ video: "" });
            addToLog( '🎬 STOP', 'warning' )       
        })
    });
}

// Default Name Loader
resetName();
