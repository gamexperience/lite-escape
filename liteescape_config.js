// Here the name to display on screen
const DEFAULT_NAME = "Lite Escape";


// Here add filename of logo set in "logos/" folder (one by line)
const logos = [
    'liteescape.png',
];


//Here add your hints (one by line)
const hints = [
    "Hint 1 : lorem ipsum dolor sit amet",
    "Hint 2 : amet sit dolor ipsum lorem"
];

//Here add your videos (one by line)
const videos = [
    "essai.mp4",
];

//here you can choose the sound for hints and videos
// files are located in "assets/media/""

const notif_hints_active = false;
const notif_hints_file = "hint.mp3";
const notif_video_active = true;
const notif_video_file = "hint.mp3"