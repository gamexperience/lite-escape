# lite-escape

Lite Escape est un mini logiciel écrit en html et javascript, fait pour fonctionner sur n'importe quel ordinateur équipé d'un navigateur, même sur des ordinateurs bloqués par une entreprise

## Demarrer

Il suffit de [télécharger lite escape](https://gitlab.com/gamexperience/lite-escape/-/archive/main/lite-escape-main.zip), ouvrir le fichier lite_escape_fr.html, cliquer sur le bouton "Panneau gestion", et vous êtes prets !

Placez l'onglet avec le timer sur un ecran visible des joueurs, et gardez l'autre onglet sur votre écran.
sur l'écran des joueurs, passez en plein écran avec le bouton "f11" de votre clavier pour un meilleur rendu !




# lite-escape

Lite software in html and javascript, to run on any computers, even on compagny locked computers 

## Getting started

Just [copy the repository](https://gitlab.com/gamexperience/lite-escape/-/archive/main/lite-escape-main.zip), open lite_escape_en.html with your browser, click on "Admin panel" button, and your good to go !

